import pandas as pd
import time

def abrirCsvDecretos(archivo):
    try:
        print('\t\t Desde: |')
        desde = input('\t>>')
        if desde == "":
            desde = 0
        else:
            desde = int(desde)
        time.sleep(2)
        expedte = pd.read_csv(archivo+".csv",names=["Expediente","Procurador"],header=0,skiprows=desde,index_col=False,encoding='utf-8')
        print("æ"*75)
        print(expedte)
        print("æ"*75)
        input("\t\t<---- Continuar con la Automatizacion ---->")
        return expedte
    except Exception as e:
        input(str(e))
        return []

def abrirMovimientosABuscarNov(archivo):
    print('\t\t Desde: |')
    desde = input('\t>>')
    if desde == "":
        desde = 0
    else:
        desde = int(desde)
    time.sleep(2)
    expedte = pd.read_csv(archivo+".csv",names=["Expediente","Operacion","Fecha"],header=0,skiprows=desde,index_col=False,encoding='utf-8')
    print("æ"*75)
    print(expedte)
    print("æ"*75)
    print("\t\t<---- Continuar con la Automatizacion ---->")
    return expedte
    
def abrirArchivoCSVExpDesconocidos(cadenaArchivo):
    time.sleep(2)
    expedteAUX = pd.read_csv(cadenaArchivo,names=["DENOM","CUIT","EXPEDIENTE"],sep=",",index_col=False,encoding='utf-8')
    print("æ"*75)
    print(expedteAUX)
    print("æ"*75)
    print("\t\t<---- Continuar con la Automatizacion ---->")
    return expedteAUX

def abrirMovimientosABuscar(archivo):
    time.sleep(2)
    expedte = pd.read_csv(archivo,names=["Expediente"],index_col=False,sep=",",encoding='utf-8')
    print("æ"*75)
    print(expedte)
    print("æ"*75)
    print("\t\t<---- Continuar con la Automatizacion ---->")
    return expedte

def abrirSubirEscritos(archivo):
    print('\t\t Desde?: |')
    desde = input('\t>>')
    if desde == "":
        desde = 0
    else:
        desde = int(desde)
    # print('\t\t Cuantos registros?: |')
    # cuantos = input('\t>>')
    # if cuantos == "":
    #     cuantos = 0
    # else:
    #     cuantos = int(cuantos)
    time.sleep(2)
    if str(archivo.split(".")[1:][0]) == "xlsx":
        expedte = pd.read_excel(archivo,names=["Expediente","Procurador","Tipo","Escrito","PdfA","PdfB"],skiprows=desde,header=None,index_col=None,encoding="utf-8")
    elif str(archivo.split(".")[1:][0]) == "csv":
        expedte = pd.read_csv(archivo,names=["Expediente","Procurador","Tipo","Escrito","PdfA","PdfB"], sep=";",skiprows=desde,index_col=False,encoding="utf-8")
    print("æ"*75)
    expedte = expedte.dropna(subset=['Expediente'])
    expedte = expedte.fillna("-")
    expedte[["Expediente"]] = expedte[["Expediente"]].astype(int)
    print(expedte)
    print("æ"*75)
    input("\t\t<---- Continuar con la Automatizacion ---->")
    return expedte
