import os
import re
import json
import time
from datetime import datetime
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pandas as pd
from tkinter import ttk, font, filedialog, messagebox
from manejoarchivos import *

#########################################
## Setup                            #####
#########################################
def introduccion():
    os.system("cls")
    print('\n\n\t\t\t\t\tBienvenido\n\n')
    time.sleep(2)
    print("\n")
    print('\t\t\t█▌▐▐▐▐ ░░░░░░░░░░░░░░░░░░░░░░░░░░▐▐▐▐▐█▌')
    print('\t\t\t\t    Automatizacion rbot-SAC')
    print('\t\t\t█▌▐▐▐▐ ░░░░░░░░░░░░░░░░░░░░░░░░░░▐▐▐▐▐█▌')
    print("\n")
    os.system("cls")


def iniciar_Proceso():
    usuario = os.environ["USERPROFILE"]

    path = "./chromedriver.exe"
    prefs = {'download.default_directory': usuario+"\\Desktop\\trabajo\\sac\\rhaniv4\\archivos",
             'download.directory_upgrade': True,
             'download.prompt_for_download': False,
             'plugins.plugins_disabled': ['Chrome PDF Viewer']}
    chromeOptions = webdriver.ChromeOptions()
    chromeOptions.add_experimental_option('prefs', prefs)
    chromeOptions.add_argument("--safebrowsing-disable-download-protection")
    chromeOptions.add_argument("--disable-popup-blocking")
    chromeOptions.add_argument("test-type")
    chromeOptions.add_argument("--disable-extensions")
    chromeOptions.add_argument('log-level=3')
    driver = webdriver.Chrome(executable_path=path, options=chromeOptions)
    return driver


def iniciar_ProcesoHeadless():
    usuario = os.environ["USERPROFILE"]

    path = "./chromedriver.exe"
    prefs = {'download.directory_upgrade': True,
             'download.prompt_for_download': False,
             'plugins.plugins_disabled': ['Chrome PDF Viewer']}
    chromeOptions = webdriver.ChromeOptions()
    chromeOptions.add_experimental_option('prefs', prefs)
    chromeOptions.add_argument("--safebrowsing-disable-download-protection")
    chromeOptions.add_argument("--disable-popup-blocking")
    chromeOptions.add_argument("test-type")
    chromeOptions.add_argument("--disable-extensions")
    chromeOptions.add_argument("headless")
    chromeOptions.add_argument('log-level=3')
    driver = webdriver.Chrome(executable_path=path, options=chromeOptions)
    return driver


def getResultados(driver):
    if not('No se encontraron datos para mostrar' in driver.find_element_by_xpath('//*[@id="Principal"]/div[2]/div[2]/div/div[1]/div[2]').text):
        descripcionExp = driver.find_elements_by_xpath(
            '//*[@id="tbBody_grdExpedientes"]/tr')
        contador = 0
        time.sleep(1)
        for i in range(len(descripcionExp)):
            print("---------------------------------------------------------------------------------------------------")
            print(descripcionExp[i].text.split("\n"))
            contador += 1
        print("---------------------------------------------------------------------------------------------------")
        return descripcionExp[i].text.split("\n")
    else:
        descripcionExp = []
        return (descripcionExp)

# _________________________________________________________________________
# MAIN FUNCTIONS
# _________________________________________________________________________

#################################ge#####################################
# Funcion para logearse
######################################################################
def doLogin(driver, comarbUsername, comarbPassword):
    usernameElement = driver.find_element_by_id('txtUserName')
    passwordElement = driver.find_element_by_id('txtUserPassword')
    buttonElement = driver.find_element_by_id("cmdLogin")

    usernameElement.send_keys(comarbUsername)
    passwordElement.send_keys(comarbPassword)
    buttonElement.click()

# ___________________________________________________________________
# NAVIGATE
# ___________________________________________________________________
def verExpediente(driver):
    driver.find_element_by_xpath(
        '//*[@id="Principal"]/div[2]/div[1]/div/div/section/div[2]/label').click()
    time.sleep(1)
    driver.find_element_by_xpath(
        '//*[@id="Principal"]/div[2]/div[1]/div/div/section/div[2]/article/p[2]/a').click()


def hayMasPag(driver):
    paginas = driver.find_element_by_xpath(
        '//*[@id="grdExpedientes"]/div[3]/div')
    if paginas.text == "":
        return False
    else:
        return True

def entrarExpediente(driver, expediente, procurador):
    """
    Obtener movimientos de un expediente.

    :Args:
        - driver - Webdriver que esta navegando.

        - expediente - El numero del expediente a subir.

        - procurador - Nombre/Legajo del procurador.

    """
    try:
        cajaExp = driver.find_element_by_xpath(
            '//*[@id="txtNumeroExpediente"]')
        cajaExp.send_keys(expediente)
        driver.find_element_by_xpath('//*[@id="btnBuscar"]').click()
        time.sleep(1)
        resExpediente = getResultados(driver)
        if (len(resExpediente) > 0):
            try:
                driver.find_element_by_xpath(
                    '//*[@id="grdExpedientes_Row_0_column7_control_0"]').click()
                time.sleep(1)
                iframe = driver.find_element_by_xpath(
                    "//iframe[@class='webDialogModalIFrame']")
                driver.switch_to.frame(iframe)
                txt = driver.find_element_by_xpath('//*[@id="lblError"]')

                if txt.text == 'LA UBICACIÓN DEL EXPEDIENTE NO PERMITE VISUALIZAR SU CONTENIDO':
                    return("ubicacion no deja", "ubicacion no deja", "ubicacion no deja")

                if txt.text == 'EL GRUPO DEL EXPEDIENTE AÚN NO ESTÁ IMPLEMENTADO':
                    return("ubicacion no deja", "ubicacion no deja", "ubicacion no deja")

                infoPrincipalExpediente = obtenerRadiografiaExpediente(driver)
                print("Radiografia:")
                print(infoPrincipalExpediente)

                operacionesExpediente = getMovimientosExpediente(
                    driver)
                print("Operaciones:")
                print(operacionesExpediente)

                llpartes = entrarPartesExpediente(
                    driver)
                print("Partes:")
                print(llpartes)

                prestamosExpediente = entrarPrestamosExpediente(
                    driver)
                print("Prestamos:")
                print(prestamosExpediente)

                remisionesExpediente = entrarRemisionesExpediente(
                    driver)
                print("Remisiones:")
                print(remisionesExpediente)
                return([infoPrincipalExpediente,operacionesExpediente])
            except Exception as e:
                print(str(e))
                return([])

    except Exception as e:
        print(str(e))
        return([])


def getMovimientosExpediente(driver):
    """
    Obtener movimientos del expediente

    :Args:
        - driver: Webdriver que esta navegando.

        - expediente - Numero de expediente a consultar.

        - procurador - Procurador asociado.

    :Return:
        - Devuelve True si ya esta subido, False si no se subio

    """
    rutaActual = os.path.dirname(os.path.abspath(__file__))
    try:
        # Hay muchos movimientos?
        morePag = driver.find_element_by_xpath(
            '//*[@id="lnkOperacionesVerMas"]')
        if "VER TODAS LAS OPERACIONES" in morePag.text:
            morePag.click()

        # Movimientos del expediente
        movExpedientes = driver.find_elements_by_xpath(
            '//*[@id="tbBody_grdOperaciones"]/tr')
        listaRes = []
        for item in movExpedientes:
            rr = item.text.split("\n")
            fecha_mov = rr[0]
            titulo_mov = rr[1]
            ubicacion_mov = rr[2]
            operacion = {
                'Fecha': fecha_mov,
                'Descripcion': titulo_mov,
                'Ubicacion': ubicacion_mov,
            }
            listaRes.append(operacion)

        return listaRes
    except Exception as e:
        print(str(e))
        return []


def getInfoExpediente(nomArchivo, usuario="", pw=""):
    """
    Metodo principal para obtener los datos del expediente.

    :Args:
        - nomArchivo - Archivo con los expedientes a buscar.

        - usuario - Nombre del usuario.

        - pw - Contraseña del usuario.

    """
    print(usuario)
    usuario = usuario.strip()
    pw = pw.strip()
    print(pw)
    dataFrameExp = abrirMovimientosABuscar(nomArchivo)

    try:
        driver = iniciar_Proceso()
        driver.get(
            'https://www.justiciacordoba.gob.ar/justiciacordoba/LogIn/Login.aspx?ReturnUrl=%2fmarcopolo%2fmenu%2findex.aspx')
        time.sleep(1)
        doLogin(driver, usuario, pw)
        time.sleep(2)
        driver.get(
            'https://www.justiciacordoba.gob.ar/marcopolo/_Expedientes/ExpedientesAlta.aspx')

        contttadios = 0

        for i in range(len(dataFrameExp)):
            os.system("cls")
            print("Fila "+str(i)+" --- Let's take it all! -- > Expediente: " +
                  str(dataFrameExp["Expediente"][i]))
            msjError = entrarExpediente(driver, str(
                dataFrameExp["Expediente"][i]), "1-33055")
            
            if len(msjError) > 0:
                time.sleep(1)
                driver.get(
                    'https://www.justiciacordoba.gob.ar/marcopolo/_Expedientes/ExpedientesAlta.aspx')
                contttadios += 1
            else:
                driver.quit()
                driver = iniciar_Proceso()
                driver.get(
                    'https://www.justiciacordoba.gob.ar/justiciacordoba/LogIn/Login.aspx?ReturnUrl=%2fmarcopolo%2fmenu%2findex.aspx')
                time.sleep(1)
                doLogin(driver, usuario, pw)
                time.sleep(2)
                verExpediente(driver)
        driver.quit()
        return ("El proceso de busqueda de movimientos para el archivo\n\t{0}, ha finalizado con exito".format(str(nomArchivo.split("/")[-1:])))
    except Exception as e:
        input(str(e))
        if '{"method":"id","selector":"txtUserName"}' in str(e):
            messagebox.showerror("ERROR!", "No se pudo acceder a la pagina, compruebe su internet.")
            driver.quit()
        elif 'unknown error: cannot determine loading status' in str(e):
            driver.quit()
            messagebox.showerror("ERROR!", "Se cerro el proceso del driver.")
        elif 'Message: no such window: target window already closed' in str(e):
            messagebox.showerror("ERROR!", "Se cerro el proceso del driver.")
        elif 'object is not subscriptable' in str(e):
            driver.quit()
            messagebox.showerror("ERROR!", "Let's take it")


def entrarPartesExpediente(driver):
    """
    Navega al tab de Partes

    :Args:
        - driver: Webdriver que esta navegando.

    """
    try:
        driver.find_element_by_id('tabPartesButtonLabel').click()
        driver.switch_to.default_content()
        time.sleep(1)

        iframe = driver.execute_script("var q = document.getElementsByTagName('body')[0].getElementsByClassName('webDialogModalIFrame'); \
                                            console.log(q[0].innerText);\
                                            return q[0];")
        driver.switch_to.frame(iframe)
        partesPrincipales = obtenerPartesPrincipales(driver)
        partesAuxiliares = obtenerPartesAuxiliares(driver)
        return [partesPrincipales,partesAuxiliares]
    except Exception as e:
        print(str(e))
        return []


def entrarPrestamosExpediente(driver):
    """
    Navega al tab de Prestamos

    :Args:
        - driver: Webdriver que esta navegando.

    """
    try:
        driver.execute_script(
            "document.getElementById('tabPrestamosButtonLabel').click()")
        time.sleep(1)
        morePag = driver.find_element_by_xpath(
            '//*[@id="lnkPrestamosVerMas"]')
        if "VER TODOS" in morePag.text:
            morePag.click()

        prestamos = obtenerPrestamos(driver)

        return prestamos
    except Exception as e:
        input(str(e))
        return []


def entrarRemisionesExpediente(driver):
    """
    Navega al tab de  Remisiones

    :Args:
        - driver: Webdriver que esta navegando.

    """
    try:
        driver.execute_script(
            "document.getElementById('tabRemisionesButtonLabel').click()")
        time.sleep(1)
        ll = obtenerRemisiones(driver)

        return ll
    except Exception as e:
        input(str(e))
        return []


def obtenerPartesPrincipales(driver):
    """
    Devuelve una lista de obj. json con las partes principales asociadas al expediente

    :Args:
        - driver: Webdriver que esta navegando.

    """
    objetos = driver.find_elements_by_xpath(
        "//*[contains(@id,'tbBody_grdPartesPrincipales')]//tr")
    listaRes = []
    for i in range(len(objetos)):
        tipoRol = objetos[i].find_element_by_xpath(
            ".//*[contains(@datafieldname,'Rol')]").text
        nombreRol = objetos[i].find_element_by_xpath(
            ".//*[contains(@datafieldname,'Nombre')]").text
        partesPrincipales = {
            'Rol': tipoRol,
            'Nombre': nombreRol
        }
        listaRes.append(partesPrincipales)
    ###################
    return listaRes


def obtenerPartesAuxiliares(driver):
    """
    Devuelve una lista de obj. json con las partes auxiliares asociadas al expediente

    :Args:
        - driver: Webdriver que esta navegando.

    """
    objetos = driver.find_elements_by_xpath(
        "//*[contains(@id,'tbBody_grdPartesAuxiliares')]//tr")
    listaRes = []
    for i in range(len(objetos)):
        tipoRol = objetos[i].find_element_by_xpath(
            ".//*[contains(@datafieldname,'Rol')]").text
        nombreRol = objetos[i].find_element_by_xpath(
            ".//*[contains(@datafieldname,'Nombre')]").text
        partesAux = {
            'Rol': tipoRol,
            'Nombre': nombreRol
        }
        listaRes.append(partesAux)
    ###################
    return listaRes


def obtenerPrestamos(driver):
    """
    Devuelve una lista de obj. json con las prestamos asociados al expediente

    :Args:
        - driver: Webdriver que esta navegando.

    """
    objetos = driver.find_elements_by_xpath(
        "//*[contains(@id,'tbBody_grdPrestamos')]//tr")
    listaRes = []
    for i in range(len(objetos)):
        fechaPrestamo = objetos[i].find_element_by_xpath(
            ".//*[contains(@datafieldname,'FechaPrestamo')]").text
        cuerpo = objetos[i].find_element_by_xpath(
            ".//*[contains(@datafieldname,'Cuerpo')]").text
        fechaDevolucion = objetos[i].find_element_by_xpath(
            ".//*[contains(@datafieldname,'FechaDevolucion')]").text
        destinatario = objetos[i].find_element_by_xpath(
            ".//*[contains(@datafieldname,'Destinatario')]").text
        prestamo = {
            'Fecha': fechaPrestamo,
            'Cuerpo': cuerpo,
            'Fecha Devolucion': fechaDevolucion,
            'Nombre': cuerpo,
            'Destinatario': destinatario,
        }
        listaRes.append(prestamo)
        # objetos[i].find_element_by_xpath(
        #     ".//*[contains(@class,'ImageButton btnVer btnSaldo')]").click()
        
    ###################
    return listaRes


def obtenerRemisiones(driver):
    """
    Devuelve una lista de obj. json con las remisiones asociados al expediente

    :Args:
        - driver: Webdriver que esta navegando.

    """
    objetos = driver.find_elements_by_xpath(
        "//*[contains(@id,'tbBody_grdRemisiones')]//tr")
    listaRes = []
    for i in range(len(objetos)):
        fecha = objetos[i].find_element_by_xpath(
            ".//*[contains(@datafieldname,'Fecha')]").text
        origen = objetos[i].find_element_by_xpath(
            ".//*[contains(@datafieldname,'Origen')]").text
        destino = objetos[i].find_element_by_xpath(
            ".//*[contains(@datafieldname,'Destino')]").text
        tiporemision = objetos[i].find_element_by_xpath(
            ".//*[contains(@datafieldname,'TipoRemision')]").text
        remision = {
            'Fecha': fecha,
            'Origen': origen,
            'Destino': destino,
            'Tipo': tiporemision
        }
        listaRes.append(remision)
    ###################
    return listaRes


def obtenerRadiografiaExpediente(driver):
    """
    Devuelve un obj. json con la informacion de cabecera del expediente

    :Args:
        - driver: Webdriver que esta navegando.

    """
    numExpediente = driver.find_element_by_id('lblNumeroExpediente').text
    numExpediente = re.sub("\D", "", numExpediente)
    tribunalActual = driver.find_element_by_id('lblTribunal').text
    fechaInicio = driver.find_element_by_id('lblFechaInicio').text
    caratula = driver.find_element_by_id('lblCaratula').text
    tipoJuicio = driver.find_element_by_id('lblTipoJuicio').text
    estado = driver.find_element_by_id('lblEstado').text
    ubicacion = driver.find_element_by_id('lblUbicacion').text
    radiografia = {
        'IdExpediente': numExpediente,
        'Caratula': caratula,
        'Dependencia': tribunalActual,
        'FechaInicio': fechaInicio,
        'Estado': estado,
        'Ubicacion': ubicacion,
        'TipoJuicio': tipoJuicio,
    }
    ###################
    return(radiografia)


getInfoExpediente("./colombero.csv", "1-33055", "nicolas.3")